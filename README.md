# apirest-master

O projeto consiste em criar uma rotina que irá carregar o arquivo SLC0001-modelo.xml que se encontra nesse repositório para um banco de dados e disponibilizar em uma API.

## Tecnologias Utilizadas
- [SpringBoot]
- [H2 Database Engine]
- [Swagger2 - Swagger-ui]
- [spring-boot-devtools]
- [lombok]
- [Maven]
- [Java 8]

# Documentação da API gerada pelo Swagger
## SLC - Serviço de Liquidação Centralizada
API REST de produtos/liquidação com Swagger-ui. Disponível em (https://apirest-master.herokuapp.com/swagger-ui.html)
```
Todos os recursos podem ser testados no Swagger 
```

# Base URL

## H2 Database Engine
```
Acesso para lista de produto no banco de dados: (https://apirest-master.herokuapp.com/h2-console/login.jsp
```
Acesso para lista de produtos em liquidação: (https://apirest-master.herokuapp.com/api/produtos)
```
Lista todos os produtos/liquicação cadatros.
```

Salvar produtos em liquidação: (https://apirest-master.herokuapp.com/api/upload)
```
Importa o arquivo com os produtos cadstrado.
```



