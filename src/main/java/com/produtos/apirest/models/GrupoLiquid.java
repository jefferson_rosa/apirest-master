package com.produtos.apirest.models;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.Data;

/**
 *
 * @author Jefferson Rosa
 * @version 1.0
 * @since 2019-09-25
 */

@Entity
@Table(name = "TB_GRUPO_LIQUIDACAO")
@Data
public class GrupoLiquid implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@JoinColumn(name = "id")
	private long id;

	private LocalDate DtLiquid;
	private long NumSeqCicloLiquid;

	

	public LocalDate getDtLiquid() {
		return DtLiquid;
	}

	public void setDtLiquid(LocalDate dtLiquid) {
		DtLiquid = dtLiquid;
	}

	

	public long getNumSeqCicloLiquid() {
		return NumSeqCicloLiquid;
	}

	public void setNumSeqCicloLiquid(long numSeqCicloLiquid) {
		NumSeqCicloLiquid = numSeqCicloLiquid;
	}



	@OneToMany(mappedBy = "produtos", cascade = CascadeType.ALL)
	private List<GrupoProduto> GrupoLiquid;

	public List<GrupoProduto> getGrupoLiquid() {
		return GrupoLiquid;
	}

	public void setGrupoLiquid(List<GrupoProduto> grupoLiquid) {
		GrupoLiquid = grupoLiquid;
	}

	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	private SegSistema segSistema;

	public SegSistema getSegSistema() {
		return segSistema;
	}

	public void setSegSistema(SegSistema segSistema) {
		this.segSistema = segSistema;
	}

}
