package com.produtos.apirest.models;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.Data;
/**
*
* @author  Jefferson Rosa
* @version 1.0
* @since   2019-09-25
*/

@Entity
@Table(name = "TB_SEG_SISTEMA")
@Data
public class SegSistema implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@JoinColumn(name = "id")
	private long id;

	private String codMsg;
	private String numCtrlSLC;
	private long iSPBIF;
	private char tpInf;
	private LocalDate DtMovto;
	private LocalDateTime dtHrSLC;

	public String getCodMsg() {
		return codMsg;
	}

	public void setCodMsg(String codMsg) {
		this.codMsg = codMsg;
	}

	public String getNumCtrlSLC() {
		return numCtrlSLC;
	}

	public void setNumCtrlSLC(String numCtrlSLC) {
		this.numCtrlSLC = numCtrlSLC;
	}

	public long getiSPBIF() {
		return iSPBIF;
	}

	public void setiSPBIF(long iSPBIF) {
		this.iSPBIF = iSPBIF;
	}

	public char getTpInf() {
		return tpInf;
	}

	public void setTpInf(char tpInf) {
		this.tpInf = tpInf;
	}

	public LocalDate getDtMovto() {
		return DtMovto;
	}

	public void setDtMovto(LocalDate dtMovto) {
		DtMovto = dtMovto;
	}

	public LocalDateTime getDtHrSLC() {
		return dtHrSLC;
	}

	public void setDtHrSLC(LocalDateTime dtHrSLC) {
		this.dtHrSLC = dtHrSLC;
	}


	@OneToOne(mappedBy = "segSistema", cascade = CascadeType.ALL)
    private GrupoLiquid grupoLiquidacao;	

	public GrupoLiquid getGrupoLiquidacao() {
		return grupoLiquidacao;
	}

	public void setGrupoLiquidacao(GrupoLiquid grupoLiquidacao) {
		this.grupoLiquidacao = grupoLiquidacao;
	}

	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	private SegControle segControle;

	public SegControle getSegControle() {
		return segControle;
	}

	public void setSegControle(SegControle segControle) {
		this.segControle = segControle;
	}
	
}
