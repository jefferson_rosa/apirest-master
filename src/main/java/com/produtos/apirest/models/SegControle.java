package com.produtos.apirest.models;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.Data;

/**
 *
 * @author Jefferson Rosa
 * @version 1.0
 * @since 2019-09-25
 */

@Entity
@Table(name = "TB_SEG_CONTROLE")
@Data
public class SegControle implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@JoinColumn(name = "id")
	private long id;

	private long identdEmissor;
	private long identdDestinatario;
	private long numSeq;
	private char indrCont;
	private String domSist;
	private String nUOp;

	public long getIdentdEmissor() {
		return identdEmissor;
	}

	public void setIdentdEmissor(long identdEmissor) {
		this.identdEmissor = identdEmissor;
	}

	public long getIdentdDestinatario() {
		return identdDestinatario;
	}

	public void setIdentdDestinatario(long identdDestinatario) {
		this.identdDestinatario = identdDestinatario;
	}

	public long getNumSeq() {
		return numSeq;
	}

	public void setNumSeq(long numSeq) {
		this.numSeq = numSeq;
	}

	public char getIndrCont() {
		return indrCont;
	}

	public void setIndrCont(char indrCont) {
		this.indrCont = indrCont;
	}

	public String getDomSist() {
		return domSist;
	}

	public void setDomSist(String domSist) {
		this.domSist = domSist;
	}

	public String getNUOp() {
		return nUOp;
	}

	public void setNUOp(String nUOp) {
		this.nUOp = nUOp;
	}

	@OneToOne(mappedBy = "segControle", cascade = CascadeType.ALL)
	private SegSistema segSistema;

	public SegSistema getSegSistema() {
		return segSistema;
	}

	public void setSegSistema(SegSistema segSistema) {
		this.segSistema = segSistema;
	}

}
