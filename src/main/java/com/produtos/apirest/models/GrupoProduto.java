package com.produtos.apirest.models;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.Data;
/**
*
* @author  Jefferson Rosa
* @version 1.0
* @since   2019-09-25
*/

@Entity
@Table(name = "TB_GRUPO_PRODUTO")
@Data
public class GrupoProduto implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@JoinColumn(name = "id")
	private long id;

	private String codProdt;

	public String getCodProdt() {
		return codProdt;
	}

	public void setCodProdt(String codProdt) {
		this.codProdt = codProdt;
	}
	@ManyToOne(cascade = CascadeType.ALL,targetEntity = GrupoLiquid.class)
    private GrupoLiquid produtos;

	public GrupoLiquid getProdutos() {
		return produtos;
	}

	public void setProdutos(GrupoLiquid produtos) {
		this.produtos = produtos;
	}
	
	
	@OneToMany(mappedBy = "liquidacaoProd", fetch=FetchType.EAGER, cascade = CascadeType.ALL)
	private List<GrupoLiquidProdt> liquidProd;

	public List<GrupoLiquidProdt> getLiquidProd() {
		return liquidProd;
	}

	public void setLiquidProd(List<GrupoLiquidProdt> liquidProd) {
		this.liquidProd = liquidProd;
	}
	

}
