package com.produtos.apirest.models;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;
/**
*
* @author  Jefferson Rosa
* @version 1.0
* @since   2019-09-25
*/

@Entity
@Table(name = "TB_GRUPO_LIQUIDACAO_PRODUTO")
@Data
public class GrupoLiquidProdt implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@JoinColumn(name = "id")
	private long id;

	private String IdentdLinhaBilat;
	private char TpDeb_Cred;
	private long ISPBIFCredtd;
	private long ISPBIFDebtd;
	private BigDecimal VlrLanc;
	private long CNPJNLiqdantDebtd;
	private String NomCliDebtd;
	private long CNPJNLiqdantCredtd;
	private String NomCliCredtd;
	private int TpTranscSLC;

	
	public String getIdentdLinhaBilat() {
		return IdentdLinhaBilat;
	}

	public void setIdentdLinhaBilat(String identdLinhaBilat) {
		IdentdLinhaBilat = identdLinhaBilat;
	}

	public char getTpDeb_Cred() {
		return TpDeb_Cred;
	}

	public void setTpDeb_Cred(char tpDeb_Cred) {
		TpDeb_Cred = tpDeb_Cred;
	}

	public long getISPBIFCredtd() {
		return ISPBIFCredtd;
	}

	public void setISPBIFCredtd(long iSPBIFCredtd) {
		ISPBIFCredtd = iSPBIFCredtd;
	}

	public long getISPBIFDebtd() {
		return ISPBIFDebtd;
	}

	public void setISPBIFDebtd(long iSPBIFDebtd) {
		ISPBIFDebtd = iSPBIFDebtd;
	}

	public BigDecimal getVlrLanc() {
		return VlrLanc;
	}

	public void setVlrLanc(BigDecimal vlrLanc) {
		VlrLanc = vlrLanc;
	}

	public long getCNPJNLiqdantDebtd() {
		return CNPJNLiqdantDebtd;
	}

	public void setCNPJNLiqdantDebtd(long cNPJNLiqdantDebtd) {
		CNPJNLiqdantDebtd = cNPJNLiqdantDebtd;
	}

	public String getNomCliDebtd() {
		return NomCliDebtd;
	}

	public void setNomCliDebtd(String nomCliDebtd) {
		NomCliDebtd = nomCliDebtd;
	}

	public long getCNPJNLiqdantCredtd() {
		return CNPJNLiqdantCredtd;
	}

	public void setCNPJNLiqdantCredtd(long cNPJNLiqdantCredtd) {
		CNPJNLiqdantCredtd = cNPJNLiqdantCredtd;
	}

	public String getNomCliCredtd() {
		return NomCliCredtd;
	}

	public void setNomCliCredtd(String nomCliCredtd) {
		NomCliCredtd = nomCliCredtd;
	}

	public int getTpTranscSLC() {
		return TpTranscSLC;
	}

	public void setTpTranscSLC(int tpTranscSLC) {
		TpTranscSLC = tpTranscSLC;
	}
	
	@ManyToOne
	private GrupoProduto liquidacaoProd;	

	public GrupoProduto getLiquidacaoProd() {
		return liquidacaoProd;
	}

	public void setLiquidacaoProd(GrupoProduto liquidacaoProd) {
		this.liquidacaoProd = liquidacaoProd;
	}


}
