package com.produtos.apirest.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.produtos.apirest.models.GrupoLiquid;
import com.produtos.apirest.models.SegSistema;
/**
*
* @author  Jefferson Rosa
* @version 1.0
* @since   2019-09-25
*/

public interface GrupoLiquidRepository extends JpaRepository<GrupoLiquid, Long> {
	GrupoLiquid findById(long id);

}
