package com.produtos.apirest.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.produtos.apirest.models.SegSistema;
/**
*
* @author  Jefferson Rosa
* @version 1.0
* @since   2019-09-25
*/

public interface SegSistemaRepository extends JpaRepository<SegSistema, Long> {
	SegSistema findById(long id);

}
