package com.produtos.apirest.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.produtos.apirest.models.GrupoProduto;
/**
*
* @author  Jefferson Rosa
* @version 1.0
* @since   2019-09-25
*/

public interface GrupoProdutoRepository extends JpaRepository<GrupoProduto, Long> {
	GrupoProduto findById(long id);

}
