package com.produtos.apirest.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.produtos.apirest.models.SegControle;
/**
*
* @author  Jefferson Rosa
* @version 1.0
* @since   2019-09-25
*/

@Repository
public interface SegControleRepository extends JpaRepository<SegControle, Long> {
	SegControle findById(long id);

}
