package com.produtos.apirest.dateTimeFormat;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
/**
*
* @author  Jefferson Rosa
* @version 1.0
* @since   2019-09-25
*/

public class Format {

	public LocalDate returnDate(String date) {		
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		LocalDate joiningDate = LocalDate.parse(date, formatter);
		return joiningDate;
	}

	public LocalDateTime returnDateTime(String dateTime) {
		dateTime = dateTime.replace("T", " ");
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
		LocalDateTime joiningDateTime = LocalDateTime.parse(dateTime, formatter);
		return joiningDateTime;
	}
}
