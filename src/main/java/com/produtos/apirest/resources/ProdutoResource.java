package com.produtos.apirest.resources;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.w3c.dom.Document;

import com.produtos.apirest.repository.GrupoLiquidRepository;
import com.produtos.apirest.repository.GrupoProdutoRepository;
import com.produtos.apirest.repository.ProdutoLiquidRepository;
import com.produtos.apirest.repository.SegControleRepository;
import com.produtos.apirest.repository.SegSistemaRepository;
import com.produtos.apirest.util.DomParseSave;
import com.produtos.apirest.util.GenerateXml;
import com.produtos.apirest.util.Validation;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 *
 * @author Jefferson Rosa
 * @version 1.0
 * @since 2019-09-25
 */

@CrossOrigin(origins = "*")
@RestController
@RequestMapping(value = "/api")
@Api(value = "API REST Produtos")
public class ProdutoResource {

	@Autowired
	ProdutoLiquidRepository prodLiq;

	@Autowired
	SegSistemaRepository segSist;

	@Autowired
	GrupoLiquidRepository grupoLiq;

	@Autowired
	GrupoProdutoRepository grupoProdut;

	@Autowired
	SegControleRepository segcont;

	@ApiOperation(value = "Retorna uma lista de Produtos")
	@GetMapping(value = "/produtos")
	public String listaProdutos() {
		
		return new GenerateXml().GenerateFile(segcont);
		
	}

	@ApiOperation(value = "Upload arquivo  produto/liquidação")
	@RequestMapping(value = "/upload", method = RequestMethod.POST)
	public String fileUpload(@RequestParam("file") MultipartFile file) throws Exception {

		try {

			Validation validation = new Validation();

			if (validation.Accept(file.getContentType())) {

				Document doc = validation.returnDoc(file);

				new DomParseSave().saveDoc(doc, prodLiq, segSist, grupoLiq, grupoProdut, segcont);

			}
		} catch (Exception e) {
			e.getMessage();
		}
		return "Xml importado com cucesso!";
	}
}
