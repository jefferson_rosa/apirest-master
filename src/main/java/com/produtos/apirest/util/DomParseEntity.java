package com.produtos.apirest.util;

import java.util.HashMap;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.produtos.apirest.dateTimeFormat.Format;
import com.produtos.apirest.models.GrupoLiquid;
import com.produtos.apirest.models.SegControle;
import com.produtos.apirest.models.SegSistema;
import com.produtos.apirest.repository.GrupoLiquidRepository;
import com.produtos.apirest.repository.SegControleRepository;
import com.produtos.apirest.repository.SegSistemaRepository;

/**
 *
 * @author Jefferson Rosa
 * @version 1.0
 * @since 2019-09-25
 */

public class DomParseEntity {


	public SegControle EntitySegControle(SegControleRepository interSegControleRepository, Document doc) {

		NodeList bCMSG = doc.getElementsByTagName("BCMSG");
		SegControle segControle = new SegControle();
		for (int temp = 0; temp < bCMSG.getLength(); temp++) {
			Node node = bCMSG.item(temp);

			if (node.getNodeType() == Node.ELEMENT_NODE) {
				Element eElement = (Element) node;
				segControle.setIdentdEmissor(Long.valueOf(eElement.getElementsByTagName("IdentdEmissor").item(0).getTextContent()));
				segControle.setIdentdDestinatario(Long.valueOf(eElement.getElementsByTagName("IdentdDestinatario").item(0).getTextContent()));
				segControle.setNumSeq(Long.valueOf(eElement.getElementsByTagName("NumSeq").item(0).getTextContent()));
				segControle.setIndrCont(eElement.getElementsByTagName("IndrCont").item(0).getTextContent().charAt(0));
				segControle.setDomSist(eElement.getElementsByTagName("DomSist").item(0).getTextContent());
				segControle.setNUOp(eElement.getElementsByTagName("NUOp").item(0).getTextContent());
			}
		}
		return interSegControleRepository.save(segControle);

	}

	public SegSistema EntitySegSistema(SegSistemaRepository inteSegSistema, Document doc, SegControle segControle) {
		NodeList sLC0001 = doc.getElementsByTagName("SLC0001");
		SegSistema segSistema = new SegSistema();
		for (int temp = 0; temp < sLC0001.getLength(); temp++) {
			Node node = sLC0001.item(temp);

			if (node.getNodeType() == Node.ELEMENT_NODE) {
				Element eElement = (Element) node;

				segSistema.setCodMsg(eElement.getElementsByTagName("CodMsg").item(0).getTextContent());
				segSistema.setDtHrSLC(new Format().returnDateTime(eElement.getElementsByTagName("DtHrSLC").item(0).getTextContent()));
				segSistema.setDtMovto(new Format().returnDate(eElement.getElementsByTagName("DtMovto").item(0).getTextContent()));
				segSistema.setiSPBIF(Long.valueOf(eElement.getElementsByTagName("ISPBIF").item(0).getTextContent()));
				segSistema.setNumCtrlSLC(eElement.getElementsByTagName("NumCtrlSLC").item(0).getTextContent());
				segSistema.setTpInf(eElement.getElementsByTagName("TpInf").item(0).getTextContent().charAt(0));
				segSistema.setSegControle(segControle);
			}
		}
		return inteSegSistema.save(segSistema);
	}

	public GrupoLiquid EntityGrupoLiquid(GrupoLiquidRepository grupoLiquidRepository, Document doc,
			SegSistema segSistema) {
		NodeList grupo_SLC0001_Liquid = doc.getElementsByTagName("Grupo_SLC0001_Liquid");
		GrupoLiquid grupoLiquid = new GrupoLiquid();
		for (int temp = 0; temp < grupo_SLC0001_Liquid.getLength(); temp++) {
			Node node = grupo_SLC0001_Liquid.item(temp);

			if (node.getNodeType() == Node.ELEMENT_NODE) {
				Element eElement = (Element) node;
				grupoLiquid.setDtLiquid(new Format().returnDate(eElement.getElementsByTagName("DtLiquid").item(0).getTextContent()));
				grupoLiquid.setNumSeqCicloLiquid(Long.valueOf(eElement.getElementsByTagName("NumSeqCicloLiquid").item(0).getTextContent()));
				grupoLiquid.setSegSistema(segSistema);
			}
		}
		return grupoLiquidRepository.save(grupoLiquid);
	}

}
