package com.produtos.apirest.util;

import java.util.ArrayList;
import java.util.List;

import com.produtos.apirest.models.GrupoLiquid;
import com.produtos.apirest.models.GrupoLiquidProdt;
import com.produtos.apirest.models.GrupoProduto;
import com.produtos.apirest.models.SegControle;
import com.produtos.apirest.models.SegSistema;
import com.produtos.apirest.repository.SegControleRepository;
import com.thoughtworks.xstream.XStream;

public class GenerateXml {
	public String GenerateFile(SegControleRepository segcont) {

		XStream xStream = new XStream();

		List<SegControle> segControl = segcont.findAll();

		List<Object> dados = new ArrayList<Object>();

		SegControle segControle = new SegControle();
		segControle.setDomSist(segControl.get(0).getDomSist());
		segControle.setIdentdDestinatario(segControl.get(0).getIdentdDestinatario());
		segControle.setIdentdEmissor(segControl.get(0).getIdentdEmissor());
		segControle.setIndrCont(segControl.get(0).getIndrCont());
		segControle.setNumSeq(segControl.get(0).getNumSeq());
		segControle.setNUOp(segControl.get(0).getNUOp());

		GrupoLiquid grupoLiquid = new GrupoLiquid();
		grupoLiquid.setDtLiquid(segControl.get(0).getSegSistema().getGrupoLiquidacao().getDtLiquid());
		grupoLiquid.setNumSeqCicloLiquid(segControl.get(0).getSegSistema().getGrupoLiquidacao().getNumSeqCicloLiquid());

		GrupoProduto grupoProduto = new GrupoProduto();

		List<GrupoProduto> v = segControl.get(0).getSegSistema().getGrupoLiquidacao().getGrupoLiquid();

		List<GrupoLiquidProdt> grupoLiquidProdts = new ArrayList<GrupoLiquidProdt>();

		for (GrupoProduto grp : v) {

			GrupoLiquidProdt grupoLiquidProdt = new GrupoLiquidProdt();

			grupoProduto.setCodProdt(grp.getCodProdt());

			grupoLiquidProdt.setCNPJNLiqdantCredtd(grp.getLiquidProd().get(0).getCNPJNLiqdantCredtd());

			grupoProduto.setLiquidProd(grupoLiquidProdts);

			grupoLiquidProdts.add(grupoLiquidProdt);

		}

		dados.add(grupoLiquidProdts);
		xStream.alias("Grupo_SLC0001_LiquidProdt", GrupoLiquidProdt.class);

		dados.add(segControle);
		xStream.alias("BCMSG", SegControle.class);

		SegSistema segSistema = new SegSistema();
		segSistema.setCodMsg(segControl.get(0).getSegSistema().getCodMsg());
		segSistema.setNumCtrlSLC(segControl.get(0).getSegSistema().getNumCtrlSLC());
		segSistema.setiSPBIF(segControl.get(0).getSegSistema().getiSPBIF());
		segSistema.setTpInf(segControl.get(0).getSegSistema().getTpInf());

		segSistema.setGrupoLiquidacao(grupoLiquid);
		xStream.alias("Grupo_SLC0001_Liquid", GrupoLiquid.class);

		segSistema.setDtHrSLC(segControl.get(0).getSegSistema().getDtHrSLC());
		segSistema.setDtMovto(segControl.get(0).getSegSistema().getDtMovto());

		dados.add(segSistema);
		xStream.alias("SLC0001", SegSistema.class);

		String xml = xStream.toXML(dados);

		return xml;
	}
}
