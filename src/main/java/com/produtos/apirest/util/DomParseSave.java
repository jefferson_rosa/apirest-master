package com.produtos.apirest.util;

import java.math.BigDecimal;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.produtos.apirest.models.GrupoLiquid;
import com.produtos.apirest.models.GrupoLiquidProdt;
import com.produtos.apirest.models.GrupoProduto;
import com.produtos.apirest.models.SegControle;
import com.produtos.apirest.models.SegSistema;
import com.produtos.apirest.repository.GrupoLiquidRepository;
import com.produtos.apirest.repository.GrupoProdutoRepository;
import com.produtos.apirest.repository.ProdutoLiquidRepository;
import com.produtos.apirest.repository.SegControleRepository;
import com.produtos.apirest.repository.SegSistemaRepository;

public class DomParseSave {

	public void saveDoc(Document doc, ProdutoLiquidRepository prodLiq, SegSistemaRepository segSist,
			GrupoLiquidRepository grupoLiq, GrupoProdutoRepository grupoProdut, SegControleRepository segcont) {

		try {
			
			doc.getDocumentElement().normalize();

			SegControle segControle = new DomParseEntity().EntitySegControle(segcont, doc);

			SegSistema segSistema = new DomParseEntity().EntitySegSistema(segSist, doc, segControle);

			GrupoLiquid grupoLiquid = new DomParseEntity().EntityGrupoLiquid(grupoLiq, doc, segSistema);

			NodeList flowList = doc.getElementsByTagName("Grupo_SLC0001_Prodt");

			for (int temp = 0; temp < flowList.getLength(); temp++) {
				Node node = flowList.item(temp);

				if (node.getNodeType() == Node.ELEMENT_NODE) {
					Element eElement = (Element) node;
					GrupoProduto grupoProduto = new GrupoProduto();

					if (eElement.getElementsByTagName("CodProdt").item(0).getTextContent() != "") {
						grupoProduto.setCodProdt(eElement.getElementsByTagName("CodProdt").item(0).getTextContent());
						grupoProduto.setProdutos(grupoLiquid);
						grupoProdut.save(grupoProduto);
					}
					grupoliquidProdSave(eElement, prodLiq, grupoProduto);
				}
			}
		} catch (Exception e) {
			e.getMessage();
			// TODO: handle exception
		}
	}

	private ProdutoLiquidRepository grupoliquidProdSave(Element eElement, ProdutoLiquidRepository prodLiq,
			GrupoProduto grupoProduto) {

		try {
			GrupoLiquidProdt grupoLiquidProdt = new GrupoLiquidProdt();
			grupoLiquidProdt.setIdentdLinhaBilat(eElement.getElementsByTagName("IdentdLinhaBilat").item(0).getTextContent());
			grupoLiquidProdt.setTpDeb_Cred(eElement.getElementsByTagName("TpDeb_Cred").item(0).getTextContent().charAt(0));
			grupoLiquidProdt.setISPBIFCredtd(Long.valueOf(eElement.getElementsByTagName("ISPBIFCredtd").item(0).getTextContent()));
			grupoLiquidProdt.setISPBIFDebtd(Long.valueOf(eElement.getElementsByTagName("ISPBIFDebtd").item(0).getTextContent()));
			grupoLiquidProdt.setVlrLanc(new BigDecimal(eElement.getElementsByTagName("VlrLanc").item(0).getTextContent()));
			grupoLiquidProdt.setCNPJNLiqdantDebtd(Long.valueOf(eElement.getElementsByTagName("CNPJNLiqdantDebtd").item(0).getTextContent()));
			grupoLiquidProdt.setNomCliDebtd(eElement.getElementsByTagName("NomCliDebtd").item(0).getTextContent());
			grupoLiquidProdt.setCNPJNLiqdantCredtd(Long.valueOf(eElement.getElementsByTagName("CNPJNLiqdantCredtd").item(0).getTextContent()));
			grupoLiquidProdt.setNomCliCredtd(eElement.getElementsByTagName("NomCliCredtd").item(0).getTextContent());
			grupoLiquidProdt.setTpTranscSLC(Integer.valueOf(eElement.getElementsByTagName("TpTranscSLC").item(0).getTextContent()));
			grupoLiquidProdt.setLiquidacaoProd(grupoProduto);
			prodLiq.save(grupoLiquidProdt);
		} catch (Exception e) {
			e.getMessage();
			// TODO: handle exception
		}
		return prodLiq;

	}

}
