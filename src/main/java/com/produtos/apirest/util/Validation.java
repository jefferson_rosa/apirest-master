package com.produtos.apirest.util;

import java.io.IOException;
import java.io.StringReader;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.springframework.web.multipart.MultipartFile;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

public class Validation {
	public Boolean Accept(String fileContentType) {

		if ("application/xml".contains(fileContentType)) {
			return true;
		}
		return false;
	}

	public Document returnDoc(MultipartFile file) throws IOException, ParserConfigurationException, SAXException {
		String fileContent = new String(file.getBytes());
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
		InputSource is = new InputSource(new StringReader(fileContent));
		return dBuilder.parse(is);
	}
}
